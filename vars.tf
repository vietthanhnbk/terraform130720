variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
    default = "us-east-2"
}
variable "amis" {
        type = map(string)
        default = {
            us-east-2 = "ami-026dea5602e368e96"
            us-east-1 = "ami-026dea5602e368e96"
        }
}
variable "PATH_TO_PRIVATE_KEY" {
    default = "mykey"
}
variable "PATH_TO_PUBLIC_KEY" {
    default = "mykey.pub"
}
variable "INSTANCE_USERNAME" {
    default = "ec2-user"
}