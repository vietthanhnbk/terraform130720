//resource "aws_key_pair" "mykey" {
//    key_name = "mykey"
//    public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
//}

resource "aws_instance" "tf5" {
    ami = "${lookup (var.amis, var.aws_region)}"
    instance_type= "t2.micro"
    key_name = "mykey"
    tags  = {
        Name = "tf5"
    }

    provisioner "file" {
        source = "script.sh"
        destination = "/tmp/script.sh"
    }

    provisioner "remote-exec" {
        inline = [
            "chmod +x /tmp/script.sh",
            "sed -i 's/\r$//' /tmp/script.sh",
            "sudo /tmp/script.sh",
            
        ]
    }
    
    connection {
        host =  "${aws_instance.tf5.public_ip }"
        user = "${var.INSTANCE_USERNAME}"
        private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
    }
    
    
    
}

